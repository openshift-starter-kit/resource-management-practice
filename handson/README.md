# キャパシティ管理ハンズオン

## アジェンダ
 0. 準備
 1. [ハンズオン①：ResourceQuotaの挙動を確認する](#ハンズオン1-resourcequotaの挙動を確認する)
 2. [ハンズオン②：ClusterResourceQuotaの挙動を確認する](#ハンズオン2-clusterresourcequotaの挙動を確認する)
 3. [ハンズオン③：LimitRangeのリソース制限を確認する](#ハンズオン3-limitrangeのリソース制限を確認する)
 4. [ハンズオン④：LimitRangeのデフォルトのリソース設定を確認する](#ハンズオン4-limitrangeのデフォルトのリソース設定を確認する)
 5. おわりに
## 0. 準備

### ユーザアカウントの割り当て
---
* Etherpad にて、user1,user2,...の箇所へ、メールアドレスを入力してください。ユーザの重複はできないので、空いている箇所へ記入をお願いします。
* **手順内に"userX"といった記載がありますが、Xの部分を自分のアカウントの番号に置き換えて実施ください。**  

## ハンズオン1. ResourceQuotaの挙動を確認する


ここではResourceQuotaを設定したProjectにおける挙動を確認します。


### このハンズオンの目的
---
* ResourceQuotaを設定することで、Project内の使用リソースを制限できることを理解する
* ResourceQuotaを超えてリソースを使用した際のログの内容を把握する


### 1-1. userXでログイン
---
WebブラウザよりOpenShift Web Conosole へアクセスします。  
※URLはEtherpadの環境情報"Openshift Console"を参照  
"userX"でログインします。  
![handson](./images/login.png)  


### 1-2. Projectの作成
---
左側メニューから"Home"→"Project"を選択します。  
![handson](./images/menu_project.png)

画面右側の"Create Project"を選択します。  
![handson](./images/create_project.png)

以下の内容を入力し、"Create"を選択します。
* Name : userX-capacity
* その他 : デフォルトのまま

![handson](./images/create_userx_capacity.png)

Project:"userX-capacity"が作成されていることを確認します。  

![handson](./images/project_detail.png)


### 1-3. ResourceQuotaの作成
---
次に作成したProjectにResourceQuotaを作成します。  
左側メニューから"Administration"→"ResourceQuotas"を選択します。  
![handson](./images/menu_resourcequota.png)

左上のProjectが"userX-capacity"になっていることを確認して、画面右上の"Create ResourceQuota"を選択します。  
![handson](./images/create_resourcequota.png)

デフォルトの設定だと少し大きいので、以下に修正して"Create"を選択します。
* request.cpu : 500m
* limit.cpu : 1 

以下のようなマニフェストになります。  
![handson](./images/create_rq_example.png)

作成すると、画面にResourceQuotaの詳細情報が表示されます。  
![handson](./images/rq_detail1.png)  

![handson](./images/rq_detail2.png)  


### 1-4. Podの作成
---

Quotaを設定したので、実際にPodを作成して挙動を確認してみましょう。  
左側メニューから"Workloads"→"Pods"を選択します。  
![handson](./images/menu_pod.png)

画面右上の"Create Pod"を選択します。  
![handson](./images/create_pod.png)

Quotaを設定したProjectでは、LimitRangeでデフォルト値を指定していない限りPod/Deploymentに要求リソースを設定しないとデプロイできません。  
試しにこのまま"Create"を選択してみましょう。すると以下のようなエラーが出力されるはずです。  
![handson](./images/pod_create_error.png)

requests/limitsをCPUとメモリーに設定しないとデプロイできないと記載されています。これはResourceQuotaにCPUとメモリーそれぞれのrequests/limitsを設定しているため発生します。

では、実際に要求リソースを設定していきましょう。  
マニフェストを以下のように書き換えて"Create"を選択してください。

```
apiVersion: v1
kind: Pod
metadata:
  name: quota-cpu-400m
  labels:
    app: httpd
  namespace: userX-capacity
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: "400m"
          memory: "256Mi"
        limits:
          cpu: "800m"
          memory: "256Mi"
```

![handson](./images/create_pod_cpu_400m.png)

実際にPodが作成されたことを確認します。  
![handson](./images/pod_detail.png)


### 1-5. ResourceQuotaの使用状況を確認
---

さて、Podがデプロイできたので、このProjectのリソースの使用状況を確認してみましょう。  
左側メニューから"Administration"→"ResourceQuotas"を選択します。  
![handson](./images/menu_resourcequota.png)

exampleを選択します。  
![handson](./images/select_resourcequota.png)

すると、ResourceQuotaで設定した値のうち、デプロイされているリソースがどれだけ使われているかを一目で確認できるようになっています。  
cpu.request 500mのうちPodに400mを、cpu.limits 1(1000m)のうちPodに800mを設定したため、それぞれの使用率が80%になっています。  
![handson](./images/rq_detail_after1.png)

画面下の方では数値での確認もできます。  
![handson](./images/rq_detail_after1_2.png)


### 1-6. ResourceQuotaの制限を超えるPodを作成
---

では、今度はResourceQuotaの制限を超えるようなPodを作成してみます。  
現在cpu.requests/cpu.limitsの余剰はそれぞれ100m/200mになります。この中で300m/600mのPodを作成してみましょう。  

左側メニューから"Workloads"→"Pods"を選択します。  
![handson](./images/menu_pod.png)  

画面右上の"Create Pod"を選択します。  
![handson](./images/create_pod_rq2.png)

先ほどと同様、resourceを設定したPodを作成しましょう。

```
apiVersion: v1
kind: Pod
metadata:
  name: quota-cpu-300m
  labels:
    app: httpd
  namespace: userX-capacity
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: "300m"
          memory: "256Mi"
        limits:
          cpu: "600m"
          memory: "256Mi"
```

この状態で"Create"を選択すると、以下のようなエラーが表示されます。  
![handson](./images/create_pod_error2.png)

"exceeded quota"とある通り、ResourceQuotaで設定したQuotaを超えた要求リソースを設定しているためエラーが出ました。  
このPodをデプロイするには、既存のPodを削除して余剰を増やすか、Quotaの上限を緩和することになります。  

ちなみにdeploymentにリソース要求を設定すると、deploymentで作成されるPod一つずつに対してこのリソース値が反映されます。そのためreplicasの値によってはデプロイエラーが発生します。実際に見てみましょう。

左側メニューから"Workloads" → "Deployment"を選択します。  
![handson](./images/menu_deployment.png)

右上から"Create Deployment"を選択します。  
![handson](./images/create_deployment.png)  

マニフェストを以下のように書き換えます。  
cpu.requestsに100mを、cpu.limitsに200mを設定しましょう。  
replicasは3のままでOKです。

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: deployment-cpu-100m
  namespace: userX-capacity
spec:
  selector:
    matchLabels:
      app: httpd
  replicas: 3
  template:
    metadata:
      labels:
        app: httpd
    spec:
      containers:
        - name: httpd
          image: >-
            image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest
          ports:
            - containerPort: 8080
          resources:
            requests:
              cpu: 100m
              memory: 256Mi
            limits:
              cpu: 200m
              memory: 256Mi
```

![handson](./images/deployment_manifest.png)

"Create"を選択すると、特にエラーなくデプロイできるかと思います。  

しかしどれだけ待ってもPodの数が1つから変化しないかと思います。  
　
![handson](./images/deployment_detail1.png)

下の方のイベントを確認すると、"ReplicaFailure"とあり、リソース不足でスケールできてない旨のエラーが表示されています。  
![handson](./images/deployment_detail2.png)

これは、現在cpu.requests/cpu.limitsの余剰はそれぞれ100m/200mあるので、Deploymentで立てられるPod1つ分だけのリソースがあることになります。  
そのため1Podだけデプロイできましたが、これでもう余剰がなくなってしまったので、2台目、3台目はデプロイされませんでした。  
このようにDeploymentを使う際はデプロイがうまくいってもスケールができない可能性があるので、希望通りのPod数に達しているかを注意してください。



## ハンズオン2. ClusterResourceQuotaの挙動を確認する


ここではClusterResourceQuotaを設定した際の挙動を確認します。  
ClusterResourceQuotaはOpenShift独自のオブジェクト（CRD）で、ResourceQuotaよりも広範囲におけるリソース制限を実現できます。  
この機能は、ひとつの開発プロジェクトで複数のProjectを利用するケースにおいて有効です。  
ではどんな形でリソース制限ができるのか、実際に触って確認してみましょう。


### このハンズオンの目的
---
* ClusterResourceQuotaを設定することで、複数のProjectにまたがったリソース制限を設定できることを理解する
* ClusterResourceQuotaを超えてリソースを使用した際のログの内容を把握する


### 2-1. Projectの作成
---
今回は複数のProjectにまたがったリソース制限を確認するので、2つのProjectを作成します。

左側メニューから"Home"→"Project"を選択します。  
![handson](./images/menu_project.png)

画面右側の"Create Project"を選択します。  
![handson](./images/create_project.png)

以下の内容を入力し、"Create"を選択します。
* Name : userX-cap-crq-1
* Display name : 空欄のまま
* Description : userX-crq

![handson](./images/create_userx_cap_crq_1.png)

同様の手順でもうひとつProjectを作成します。  
Project作成画面にて以下の内容を入力し、"Create"を選択します。
* Name : userX-cap-crq-2
* Display name : 空欄のまま
* Description : userX-crq

![handson](./images/create_userx_cap_crq_2.png)

作成されたProjectのYALMを見てみると、metadata.annotationsに以下のような記載があるのがわかります。この記載を覚えておきましょう。
* openshift.io/description: userX-crq

![handson](./images/project_yaml.png)


### 2-2. ClusterResourceQuotaの作成
---

次にClusterResourceQuotaを作成します。  
左側メニューの"Administration" → "CustomResourceDefinitions"を選択します。

![handson](./images/menu_crd.png)

検索欄に"clusterresourcequota"と入力すると、結果にClusterResourceQuotaが表示されるので選択します。

![handson](./images/search_crd.png)

"Instances"を選択します。

![handson](./images/select_crq_instances.png)

"Create ClusterResourceQuota"を選択します。

![handson](./images/select_create_crq.png)

ここでマニフェストを以下のように記載します。  
selector.annotationsに、先ほど確認したannotationの値を記載します。selectorの名の通り、このClusterResourceQuotaは"openshift.io/description: userX-crq"というannotationを持つProjectを対象とします。


```
apiVersion: quota.openshift.io/v1
kind: ClusterResourceQuota
metadata:
  name: userX-crq
spec:
  quota:
    hard:
      requests.cpu: "500m"
      limits.cpu: "1"
      requests.memory: "1Gi"
      limits.memory: "2Gi"
  selector:
    annotations: 
      openshift.io/description: userX-crq
```

![handson](./images/crq_manifest.png)

作成するとResourceQuotaに似たような詳細画面が確認できます。

![handson](./images/crq_detail1.png)


### 2-3. Podの作成
---

ではPodを作成していきましょう。まずはProject:userX-cap-cqr-1にデプロイします。  
左側メニューから"Workloads"→"Pods"を選択します。  
![handson](./images/menu_pod.png)

左上のProjectを"userX-cap-crq-1"に変更し、画面右上の"Create Pod"を選択します。  
![handson](./images/create_pod_cap_crq_1.png)
  
マニフェストを以下のように書き換えて"Create"を選択してください。

```
apiVersion: v1
kind: Pod
metadata:
  name: quota-cpu-400m
  labels:
    app: httpd
  namespace: userX-cap-crq-1
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: "400m"
          memory: "256Mi"
        limits:
          cpu: "800m"
          memory: "256Mi"
```

![handson](./images/create_pod_cpu_400m_cap_crq_1.png)

無事にデプロイできたかと思います。

![handson](./images/pod_detail_cpu_400m_cap_crq_1.png)

次にuserX-cap-crq-2にPodを作成してみます。
先ほどと同様にProject:userX-cap-crq-2に変更し、画面右上の"Create Pod"を選択します。  
![handson](./images/create_pod_cap_crq_2.png)

マニフェストを以下のように書き換えて"Create"を選択してください。

```
apiVersion: v1
kind: Pod
metadata:
  name: quota-cpu-400m
  labels:
    app: httpd
  namespace: userX-cap-crq-2
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: "400m"
          memory: "256Mi"
        limits:
          cpu: "800m"
          memory: "256Mi"
```

"Create"を選択すると、以下のようなエラーが表示されます。

![handson](./images/crq_error.png)

表示文言はResourceQuotaと同様ですが、userX-cap-crq-2にはPodをひとつもデプロイしていないのにリソース不足のエラーが出ました。  
これにより、ClusterResourceQuotaが2つのProjectにまたがってリソース制限がされていることがわかったかと思います。

ちなみにClusterResourceQuotaは全Projectの"ResourceQuota"の画面に表示されます。  
Filterを使うことでClusterResourceQuotaを非表示にすることも可能です。

![handson](./images/crq_view.png)

今回は画面上で設定可能な"Description"の値を条件に指定しましたが、基本的には開発プロジェクトに払い出すProjectにて、metadata.labelsで開発プロジェクト固有のkey:valueを設定し、selector.matchLabelsにて指定する形が主流になります。

以下のようなClusterResourceQuotaマニフェストの場合は、metadata.labelsに"app:frontend"を含むProjectがリソース制限の対象になります。

```
apiVersion: quota.openshift.io/v1
kind: ClusterResourceQuota
metadata:
  name: userX-crq-label
spec:
  quota:
    hard:
      requests.cpu: "500m"
      limits.cpu: "1"
      requests.memory: "1Gi"
      limits.memory: "2Gi"
  selector:
    labels:
      matchLabels:
        app: frontend 
```

このように、ClusterResourceQuotaを使えば複数のProjectにまたがってリソース制限をかけられるため、より柔軟なキャパシティ管理を実現できます。  
OpenShiftを利用する際は是非この機能も活用してみてください。

## ハンズオン3. LimitRangeのリソース制限を確認する


ここではProjectに対してLimitRangeを設定した際の挙動を確認します。  
LimitRangeはオブジェクトひとつ当たりに対するリソース設定の最大量を指定できます。  
講義でお話したように、LimitRangeはPodのrequests/limitsだけでなくImageのサイズやPVCの要求サイズなど様々な値の制限が可能です。  
ただし、ここではハンズオンとしてPodのRequests/Limitsを設定し、その挙動を確認してみます。


### このハンズオンの目的
---
* LimitRangeを設定することで、1オブジェクト当たりのリソース制限を設定できることを理解する
* LimitRangeを超えてリソース設定を実施した際のエラーログの内容を把握する



### 3-1. Projectの作成
---
このハンズオン用に新たにProjectを作成します。

左側メニューから"Home"→"Project"を選択します。  
![handson](./images/menu_project.png)

画面右側の"Create Project"を選択します。  
![handson](./images/create_project.png)

以下の内容を入力し、"Create"を選択します。
* Name : userX-cap-limits
* その他 : 空欄のまま

![handson](./images/create_userx_cap_limits.png)


### 3-2. LimitRangeの作成
---

続いてProjectにLimitRangeを設定します。
左側メニューから"Administration" → "LimitRanges"を選択します。

![handson](./images/menu_limits.png)

左上のProjectが"userX-cap-limits"になっていることを確認して、画面右上の"Create LimitRange"を選択します。

![handson](./images/create_limits.png)

するとLimitRangeのマニフェストが表示されます。  
今回は以下のように変更してください。
```
apiVersion: v1
kind: LimitRange
metadata:
  name: mem-limit-range
  namespace: userX-cap-limits
spec:
  limits:
    - min:
        memory: 256Mi
      max:
        memory: 1Gi
      type: Pod
```
中身ですが、memoryに対し"limits.min"と"limits.max"をそれぞれ256Mi/1Giで設定しています。
"limits.min"は1Podにおける"resource.request"の下限値です。要するに、memoryのRequestsを256Miより低い値に設定することはできなくなります。  
一方で、"limits.max"は1Podにおける"resource.limits"の上限値です。この2つの値を覚えておいて下さい。

![handson](./images/limits_yaml1.png)

"Create"を選択するとLimitRangeが作成され、詳細画面が表示されます。
画面下部にて設定値が確認できます。

![handson](./images/limits_view1.png)  
![handson](./images/limits_view2.png)  


### 3-3. podの作成
---

さて、LimitRangeもかけたのでPodを作成してみましょう。

左側メニューから"Workloads"→"Pods"を選択します。  
![handson](./images/menu_pod.png)

左上のProjectが"userX-cap-limits"になっていることを確認して、画面右上の"Create Pod"を選択します。  
![handson](./images/create_pod_limits.png)

Podのマニフェストを以下に書き換えます。  
memoryのrequestsを512Miに、limitsを1Giにしています。
```
apiVersion: v1
kind: Pod
metadata:
  name: quota-mem-512mi
  labels:
    app: httpd
  namespace: userX-cap-limits
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: "300m"
          memory: "512Mi"
        limits:
          cpu: "600m"
          memory: "1Gi"
```
![handson](./images/pod_limits_yaml1.png)

"Create"を押すと、問題なくデプロイできたかと思います。  
これはLimitRange内に収まる値で設定しているので特にエラーなくデプロイできました。

では次に下限値を下回るPodをデプロイしてみましょう。

先ほどと同様の手順でCreate Pod画面にいき、以下のマニフェストを記載します。
```
apiVersion: v1
kind: Pod
metadata:
  name: quota-mem-128mi
  labels:
    app: httpd
  namespace: userX-cap-limits
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: "300m"
          memory: "128Mi"
        limits:
          cpu: "600m"
          memory: "1Gi"
```

![handson](./images/pod_limits_yaml2.png)

"Create"を選択すると、以下のようなエラーが表示されます。

![handson](./images/pod_limits_error1.png)

これはLimitRangeの下限値を下回った設定の場合に表示されるエラーです。  
うまく制御できていることが確認できます。

では同じ画面で以下のようにマニフェストを書き直して、再度"Create"してみましょう。  
requests.memoryを256Miに上げて、limits.memoryを2Giに変更しています。

```
apiVersion: v1
kind: Pod
metadata:
  name: quota-mem-2gi
  labels:
    app: httpd
  namespace: userX-cap-limits
spec:
  containers:
    - name: httpd
      image: 'image-registry.openshift-image-registry.svc:5000/openshift/httpd:latest'
      ports:
        - containerPort: 8080
      resources:
        requests:
          cpu: "300m"
          memory: "256Mi"
        limits:
          cpu: "600m"
          memory: "2Gi"
```

![handson](./images/pod_limits_yaml3.png)

今度はLimitRangeの上限値を超えている旨のエラーが表示されました。  

![handson](./images/pod_limits_error2.png)

このように、LimitRangeを使うと1Pod当たりのリソース使用量を制限できるため、ひとつの大きなPodをデプロイしてリソースを食い潰すといった現象を防ぐことができます。  

実はこの機能だけでなく、LimitRangeはもうひとつ便利な機能を持っています。  
次のハンズオンではこの部分にフォーカスします。

## ハンズオン4. LimitRangeのデフォルトのリソース設定を確認する
---

ここでは、LimitRangeが持つ「Podに自動的にリソース値を設定する」機能を試してみます。  
ハンズオン1で少し触れましたが、ResourceQuotaを設定したProjectに対してPodをデプロイする場合、resources.requestsやresources.limitsを設定しないとデプロイできません。  
ただ検証や確認のために簡易的なPodを立てる時、毎回リソース要求を設定するのも煩わしいことがあります。  
そこでLimitRangeにてデフォルトのリソース要求を定義することで、PodやDeploymentにリソース要求を設定していなくても、デフォルトでLimitRangeで設定した値を反映させることができます。  
少しイメージがしづらいかもしれませんので、実際に触ってみましょう。


### このハンズオンの目的
---
* LimitRangeのデフォルト設定を利用することで、PodやDeploymentに対して自動的にリソース要求を設定できることを理解する


### 4-1. Projectの作成
---
このハンズオン用に新たにProjectを作成します。

左側メニューから"Home"→"Project"を選択します。  
![handson](./images/menu_project.png)

画面右側の"Create Project"を選択します。  
![handson](./images/create_project.png)

以下の内容を入力し、"Create"を選択します。
* Name : userX-cap-limits-default
* その他 : 空欄のまま

![handson](./images/create_userx_cap_limits_default.png)


### 4-2. ResourceQuotaの作成
---
Podへリソース要求しないとデプロイできないように、Projectに対してResourceQuotaを設定します。  
左側メニューから"Administration"→"ResourceQuotas"を選択します。  
![handson](./images/menu_resourcequota.png)

左上のProjectが"userX-cap-limits-default"になっていることを確認して、画面右上の"Create ResourceQuota"を選択します。  
![handson](./images/create_resourcequota2.png)

今回はリソースを超えるようなリソースをデプロイしないため、デフォルトの設定のまま"Create"を選択します。

![handson](./images/create_rq_example2.png)

これで準備はOKです。  
![handson](./images/rq_detail3.png)  


### 4-3. LimitRangeの作成
---

では実際にProjectにLimitRangeを設定してみましょう。  
左側メニューから"Administration" → "LimitRanges"を選択します。

![handson](./images/menu_limits.png)

左上のProjectが"userX-cap-limits-default"になっていることを確認して、画面右上の"Create LimitRange"を選択します。

![handson](./images/create_limits2.png)

するとLimitRangeのマニフェストが表示されます。  
今回は以下のように変更してください。
```
apiVersion: v1
kind: LimitRange
metadata:
  name: default-limit-range
  namespace: userX-cap-limits-default
spec:
  limits:
    - defaultRequest:
        cpu: 100m
        memory: 256Mi
      default:
        cpu: 500m
        memory: 512Mi
      type: Container
```
"limits.defaultRequest"と"limits.default"をそれぞれ設定しています。
ちょっとわかりづらいですが、"limits.defaultRequest"はPodやDeploymentにデフォルトで設定する"resources.requests"の値です。一方で"limits.default"は"resources.limits"の値になります。  
一点注意いただきたいのが、ここで設定するtypeは"Pod"ではなく"Conitaner"になります。  
resources.requests/resources.limitsはPod内のContainerひとつひとつに設定する値のため、ここは"Container"を指定してください。

![handson](./images/limits_yaml2.png)

"Create"を選択するとLimitRangeが作成され、詳細画面が表示されます。
画面下部にて設定値が確認できます。

![handson](./images/limits_default_view1.png)  
![handson](./images/limits_default_view2.png)  


### 4-4. Podの作成
---

最後にPodをデプロイして挙動を確認してみましょう。  
左側メニューから"Workloads"→"Pods"を選択します。
![handson](./images/menu_pod.png)

左上のProjectが"userX-cap-limits-default"になっていることを確認し、画面右上の"Create Pod"を選択します。  
![handson](./images/create_pod_limits_default.png)  

マニフェストの画面にはデフォルトでexample Podの記載がありますが、このマニフェストにはresroucesの設定はありません。  
本来ならResrouceQuotaが設定されているProjectにはこのPodがデプロイされないはずです。  
では"Create"を選択してください。

![handson](./images/pod_limits_default_yaml.png)  

いかがでしょうか。デプロイできたかと思います。  
ではデプロイされたPodのyamlを見てみましょう。タブのYAMLを選択します。

![handson](./images/pod_limits_default_detail.png)  

yamlの131行目あたりに、LimitRangeでデフォルト設定したリソース要求が反映されていることが確認できます。  
このようにLimitRangeを活用することで、自動的にリソース要求を設定することができるようになります。

![handson](./images/pod_limits_default_yaml_view.png)  

ちなみにMetricsでもリソース要求の値が確認できます。

![handson](./images/pod_limits_default_metrics_view1.png)  
![handson](./images/pod_limits_default_metrics_view2.png)  


## おわりに

以上でキャパシティ管理のハンズオンは終了です。  
これらは基本的に開発プロジェクトにProjectを払い出す際、インフラ運用者側で事前に設定するケースが多いかと思います。  
今回はハンズオンのためGUIをベースに実施しましたが、運用の自動化を進める際は今回の作業を全てマニフェスト（yamlファイル）で定義することが求められます。  
まずは今回の作業の目的やデプロイしているオブジェクトの概要を理解し、慣れてきたらマニフェストでのデプロイも是非試してみてください。  
お疲れ様でした。

